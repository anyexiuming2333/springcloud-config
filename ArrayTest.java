package com.java.test.array;

import org.junit.Test;

import java.util.Scanner;

/**
 * @author 李嘉图
 * @date 2020/10/17 0:53
 */
public class ArrayTest {

    public static void main(String[] args)  {
        //测试杨辉三角,键盘输入,不能单元测试
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入三角形的最大行数：");
        int n= sc.nextInt();

        //新建二维数组
        int[][] array=new int[n][n];

        //将边框的数全部为1
        for (int i = 0; i < array.length; i++) {
            array[i][0]=1;
            array[i][i]=1;
        }
        //核心规则，内部的元素值=头上元素+头上左边元素
        for (int i = 2; i < array.length; i++) {
            for (int j = 1; j <array[i].length ; j++) {
                array[i][j]=array[i-1][j]+array[i-1][j-1];
            }
        }

        //遍历数组
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j <=i ; j++) {
                System.out.print(array[i][j]+" "+" ");
            }
            System.out.println();
        }


    }
}
